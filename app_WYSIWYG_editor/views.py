from django.shortcuts import render
from django.http import HttpResponse
from django.template import RequestContext
from django.shortcuts import render_to_response

from django import forms
from froala_editor.widgets import FroalaEditor

from django.db import models
from froala_editor.fields import FroalaField



def index(request):
    context = RequestContext(request)
    context_dict = {'modulo': "Editor" }
    return render_to_response('app_WYSIWYG_editor/index.html', context_dict, context)

class Page(models.Model):
    content = FroalaField(theme='dark')
