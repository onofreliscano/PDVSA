<!DOCTYPE html>

<html class="no-js" lang="es">
<head>
    <title>SIGRH - Plan de Jubilación II Semestre CVP y EEMM</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <style>
    #myInput {
    background-image: url('/css/searchicon.png'); /* Add a search icon to input */
    background-position: 10px 12px; /* Position the search icon */
    background-repeat: no-repeat; /* Do not repeat the icon image */
    width: 100%; /* Full-width */
    font-size: 16px; /* Increase font-size */
    padding: 12px 20px 12px 40px; /* Add some padding */
    border: 1px solid #ddd; /* Add a grey border */
    margin-bottom: 12px; /* Add some space below the input */
}

#myTable {
    border-collapse: collapse; /* Collapse borders */
    width: 100%; /* Full-width */
    border: 1px solid #ddd; /* Add a grey border */
    font-size: 18px; /* Increase font-size */
}

#myTable th, #myTable td {
    text-align: left; /* Left-align text */
    padding: 12px; /* Add padding */
}

#myTable tr {
    /* Add a bottom border to all table rows */
    border-bottom: 1px solid #ddd;
}

#myTable tr.header, #myTable tr:hover {
    /* Add a grey background color to the table header and on hover */
    background-color: #f1f1f1;
}


   </style>

<script type="text/javascript">
Paginador = function(divPaginador, tabla, tamPagina)
{
    this.miDiv = divPaginador; //un DIV donde irán controles de paginación
    this.tabla = tabla;           //la tabla a paginar
    this.tamPagina = tamPagina; //el tamaño de la página (filas por página)
    this.pagActual = 1;         //asumiendo que se parte en página 1
    this.paginas = Math.floor((this.tabla.rows.length - 1) / this.tamPagina); //¿?

    this.SetPagina = function(num)
    {
        if (num < 0 || num > this.paginas)
            return;

        this.pagActual = num;
        var min = 1 + (this.pagActual - 1) * this.tamPagina;
        var max = min + this.tamPagina - 1;

        for(var i = 1; i < this.tabla.rows.length; i++)
        {
            if (i < min || i > max)
                this.tabla.rows[i].style.display = 'none';
            else
                this.tabla.rows[i].style.display = '';
        }
        this.miDiv.firstChild.rows[0].cells[1].innerHTML = this.pagActual;
    }

    this.Mostrar = function()
    {
        //Crear la tabla
        var tblPaginador = document.createElement('table');

        //Agregar una fila a la tabla
        var fil = tblPaginador.insertRow(tblPaginador.rows.length);

        //Ahora, agregar las celdas que serán los controles
        var ant = fil.insertCell(fil.cells.length);
        ant.innerHTML = 'Anterior';
        ant.className = 'pag_btn'; //con eso le asigno un estilo
        var self = this;
        ant.onclick = function()
        {
            if (self.pagActual == 1)
                return;
            self.SetPagina(self.pagActual - 1);
        }

        var num = fil.insertCell(fil.cells.length);
        num.innerHTML = ''; //en rigor, aún no se el número de la página
        num.className = 'pag_num';

        var sig = fil.insertCell(fil.cells.length);
        sig.innerHTML = 'Siguiente';
        sig.className = 'pag_btn';
        sig.onclick = function()
        {
            if (self.pagActual == self.paginas)
                return;
            self.SetPagina(self.pagActual + 1);
        }

        //Como ya tengo mi tabla, puedo agregarla al DIV de los controles
        this.miDiv.appendChild(tblPaginador);

        //¿y esto por qué?
        if (this.tabla.rows.length - 1 > this.paginas * this.tamPagina)
            this.paginas = this.paginas + 1;

        this.SetPagina(this.pagActual);
    }
}
</script>



</head>



    <body class="impress-not-supported" >

<?php include '../includes/css-js.html';?>
<?php include '../includes/header_presentation.html';?>


        <?php
        $servername = "localhost";
        $username = "root";
        $password = "db777";
        $dbname = "db_presentacion_jubilados";

        // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);

        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
            //echo "BAD";
            }
            {
             //echo "GOOD";
            }
?>

<div id="impress">

<!----------------- SLIDE #1 ------------------>

   <!-- <div class="step" data-x="500" data-y="1" data-scale="4.5">
        <center>
        <span class="fonts_title_presentation_main">
            Gerencia de RRHH CVP
            <br>

        </span>
        </center>
    </div>-->

<!----------------- SLIDE #2 ------------------>
<!--<a href="../../static/downloads/Presentacion_SIGRH.pdf" class="fonts_link_slide_pdvsa" target="_blank">Descargar presentación en formato PDF</a>-->
<!--
    <div class="step" data-x="0" data-y="400" data-scale="0.5" data-rotate="90" >
        <center>
        <span class="fonts_title_presentation_main">
             Plan de Jubilación. II Semestre 2017
             CVP y Empresas Mixtas<br>
             Gerencia de RRHH CVP
        </span><br>

        </center>
    </div>-->

<div class="step" data-x="0" data-y="400" data-scale="1" data-rotate="90" >
        <center>
        <span class="fonts_title_presentation_main">
             Plan de Jubilación. II Semestre 2017
             CVP y Empresas Mixtas<br>
             Gerencia de RRHH CVP
        </span><br>

        </center>
    </div>

<!----------------- SLIDE #3 ------------------>

    <div id = "INFO" class="step slide_PDVSA" data-x="0" data-y="-1500">
 <p align="justify">
        <span class="fonts_content_presentation_main">
         <br>
            <br>
        <strong>Objetivo</strong>
            <br>
            <br>

               Informar a la Junta Directiva de la CVP, la proyección del II Semestre de los trabajadores de la CVP
               y Empresas Mixtas que al alcanzar la edad de retiro cumplirán con los criterios de elegibilidad para
               hacerse acreedores al beneficio de jubilación, conforme con lo establecido en la norma y disposiciones del Plan.


            </span>
            </p>
    </div>

<!----------------- SLIDE #2 ------------------>

    <div id = "inquitudes" class="step slide_PDVSA " data-x="1000" data-y="-1500">

    <p align="justify">
        <span class="fonts_content_presentation_main">

            <br>
        <strong>Antecedentes</strong>
            <br>
            <br>

               Desde que CVP asumió el proceso de Jubilación se establecieron la líneas de acción para dar estricto
               cumplimiento al Plan de Jubilación de los trabajadores de la CVP y de las Empresas Mixtas  inmediatamente
               alcance la edad de retiro, años de servicios y aportes al CCI, conforme a los criterios de elegibilidad
               establecidos en las normativas internas.

            </span>
            </p>

    </div>

<!----------------- SLIDE #3 ------------------>

    <div id = "necesidades" class="step slide_PDVSA" data-x="2000" data-y="-1500">
          <br>
        <strong>Inquietudes y necesidades</strong>
         <br> <br>
         <canvas id="chartjs-5" style="height:95vh; width:105vw"></canvas>
         <script>

                $(document).ready(function() {
                    $(document).on('impress:stepenter', function(e) {
                        var currentSlide = $(e.target).attr('id');

                        if (currentSlide === 'slide2') {
                            setTimeout(api.next, 2000);
                        } else if (currentSlide === 'necesidades') {
                            //Inicio del chart
                            new Chart(document.getElementById("chartjs-5"),
                                {"type":"polarArea",
                                 "data":

                                    {"labels":["Inconformidad: con el monto de la pensión - 100%","Choque emocional: cambio de condición activo-jubilado - 80%","Exigencia: paseos por parte de la empresa - 50%","Inconformidad: tiempo de notificación de la jubilación - 30%"],"datasets":[
                                    {"label":"Inquietudes","data":[30,100,80,50],
                                        "backgroundColor":["rgb(255, 99, 132)",

                                        "rgb(255, 205, 86)",
                                        "rgb(201, 203, 207)",
                                        "rgb(54, 162, 235)",
                                        ]},
                                    ]},
                                    //inicio options
                                    options: {
                                            legend: {
                                                display: true,
                                                position: "left",
                                                labels:
                                                {
                                                padding: 12,
                                                fontSize: 17,
                                                fillStyle: "#5F3A0B",
                                                fontColor: 'rgb(102, 102, 102)',
                                                boxWidth: 10,

                                                },

                                            },
                                            }


                                    //fin options
                            });

                            //Fin del chart
                        }
                    });
                });


    </script>


    </div>

    <!----------------- SLIDE #4 ------------------>

    <div id = "proyeccion1" class="step slide_PDVSA" data-x="3000" data-y="-1500">

    <?php
  $sql = $conn->query("SELECT COUNT(*) FROM tbl_trabajadores_tipo_caso_EEMM");
$row = $sql->fetch_row();
$count = $row[0];


$sql = $conn->query("SELECT COUNT(*) FROM tbl_trabajadores_tipo_caso_EEMM WHERE tbl_trabajadores_tipo_caso_EEMM_org_eemm <> 'CVP' ");
$row = $sql->fetch_row();
$countEEMM = $row[0];
//echo  "CVP" ." ". $countEEMM ;

$sql = $conn->query("SELECT COUNT(*) FROM tbl_trabajadores_tipo_caso_EEMM WHERE tbl_trabajadores_tipo_caso_EEMM_org_eemm = 'CVP' ");
$row = $sql->fetch_row();
$countCVP = $row[0];
//echo  "CVP" ." ". $countCVP ;

$percentageEEMM = number_format($countEEMM*100/$count);

$percentageCVP = number_format($countCVP*100/$count);

        ?>

        <br>
    <strong>Proyección II Semestre. Total trabajadores: </strong>
    <?php
    echo " ". $count ;
    ?>
    <br>
    <br>

<canvas id="chartjs-4" class="chartjs" style="height:200vh; width:250vw"></canvas>

     <script>

                $(document).ready(function() {
                    $(document).on('impress:stepenter', function(e) {
                        var currentSlide = $(e.target).attr('id');

                        if (currentSlide === 'slide2') {
                            setTimeout(api.next, 2000);
                        } else if (currentSlide === 'proyeccion1') {
                            //Inicio del chart


                           new Chart(document.getElementById("chartjs-4"),
                            {"type":"doughnut","data":{"labels":["Empresas Mixtas <?php echo $percentageEEMM; ?>%","CVP <?php echo $percentageCVP; ?>%"],
                            "datasets":
                            [{"label":"My First Dataset",
                            "data":["<?php echo $countEEMM; ?>","<?php echo $countCVP; ?>"],
                            "backgroundColor":["rgb(255, 75, 75)",
                            "rgb(128, 0, 0)","rgb(128, 0, 0)"]}
                            ]},

                            //inicio options
                                    options: {
                                            legend: {
                                                display: true,
                                                position: "left",
                                                labels:
                                                {
                                                padding: 15,
                                                fontSize: 20,
                                                fillStyle: "#5F3A0B",
                                                fontColor: 'rgb(102, 102, 102)',
                                                boxWidth: 20,

                                                },

                                            },
                                            }


                                    //fin options


                        });
                            //Fin del chart
                        }
                    });
                });


    </script>

    </div>


<!----------------- SLIDE #5 ------------------>

    <div id = "proyeccion2" class="step slide_PDVSA" data-x="4000" data-y="-1500">
    <br>
    <strong>Proyección II Semestre. Total Trabajadores: </strong>

<?php
  $sql = $conn->query("SELECT COUNT(*) FROM tbl_trabajadores_tipo_caso_EEMM");
$row = $sql->fetch_row();
$count = $row[0];
echo " ". $count ;

$sql = $conn->query("SELECT COUNT(*) FROM tbl_trabajadores_tipo_caso_EEMM WHERE tbl_trabajadores_tipo_caso_EEMM_org_eemm <> 'CVP' ");
$row = $sql->fetch_row();
$countEEMM = $row[0];
//echo  "CVP" ." ". $countEEMM ;

$sql = $conn->query("SELECT COUNT(*) FROM tbl_trabajadores_tipo_caso_EEMM WHERE tbl_trabajadores_tipo_caso_EEMM_org_eemm = 'CVP' ");
$row = $sql->fetch_row();
$countCVP = $row[0];
//echo  "CVP" ." ". $countCVP ;

        ?>



 <br>
 <br>

    <canvas id="chartjs-1" class="chartjs" style="height:210vh; width:250vw"></canvas>
    <script>
    $(document).ready(function() {
                    $(document).on('impress:stepenter', function(e) {
                        var currentSlide = $(e.target).attr('id');

                        if (currentSlide === 'slide2') {
                            setTimeout(api.next, 2000);
                        } else if (currentSlide === 'proyeccion2') {
                            //Inicio del chart


        new Chart(document.getElementById("chartjs-1").getContext("2d"),
            {"type":"bar","data":
                {"labels":["No Contractual","Contractual M","Contractual D"],
                "datasets":[
                {"label":"Total de trabajadores EEMM: <?php echo $countEEMM; ?>","data":[129,26,57],
                "fill":false,
                "backgroundColor":[
                "rgb(255, 205, 86)",
                "rgb(201, 162, 235)",
                "rgb(54, 162, 235)"],
                "borderColor":["rgb(255, 99, 132)","rgb(255, 159, 64)",
                "rgb(255, 205, 86)","rgb(75, 192, 192)","rgb(54, 162, 235)",
                "rgb(153, 102, 255)","rgb(201, 203, 207)"],"borderWidth":2},

                {"label":"Total de trabajadores CVP: <?php echo $countCVP; ?>","data":[13,0,1],
                "fill":false,
                "backgroundColor":[
                "rgb(255, 205, 86)",
                "rgb(201, 162, 235)",
                 "rgb(54, 162, 235)"],
                "borderColor":["rgb(255, 99, 132)","rgb(255, 159, 64)",
                "rgb(255, 205, 86)","rgb(75, 192, 192)","rgb(54, 162, 235)",
                "rgb(153, 102, 255)","rgb(201, 203, 207)"],"borderWidth":2},




            ]},
            "options":




            {



                                    legend: {
                                                display: true,
                                                position: "bottom",
                                                labels:
                                                {
                                                padding: 30,
                                                fontSize: 17,
                                                fillStyle: "#5F3A0B",
                                                fontColor: 'rgb(102, 102, 102)',
                                                boxWidth: 0,


                                                },

                                            },


                                "scales":
                                {"yAxes":
                                [{"ticks":
                                    {"beginAtZero":true},
                                }]
                                }



                }


            });


         //Fin del chart
                        }
                    });
                });
    </script>

    </div>





    <div id="ACCION1" class="step slide_PDVSA" data-x="5000" data-y="-1500">

 <p align="justify">
        <span class="fonts_content_presentation_main">

            <br>
        <strong>Acciones</strong>
            <br>
            <br>

      <!--         Informar a la Junta Directiva de la CVP, la proyección del II Semestre de los trabajadores de la CVP y Empresas Mixtas que al alcanzar la edad de retiro cumplirán con los criterios de elegibilidad para hacerse acreedores al beneficio de jubilación, conforme con lo establecido en la norma y disposiciones  del Plan.-->

Se solicita el respaldo de las siguientes acciones para el estricto cumplimiento del Plan Anual de Jubilación de la CVP y las Empresas Mixtas:

<br>
            <br>

               1. Instruir  a las Empresas Mixtas a cumplir con la notificación de la jubilación al trabajador con al menos 90 días de antelación a la fecha efectiva de jubilación.
              <br>  <br>

            </span>
            </p>

    </div>


    <div id="ACCION2" class="step slide_PDVSA" data-x="6000" data-y="-1500">

 <p align="justify">
        <span class="fonts_content_presentation_main">

            <br>
       <strong>Acciones</strong><br><br>

              2. Implementar un programa de preparación para la jubilación que contribuya a la incorporación positiva a la nueva condición.
              <br>  <br>



            3.  En aquellos casos en los que el trabajador se niega rotundamente a aceptar el plan de jubilación y la organización  no respalda un diferimiento, ejecutar la medida de terminación de servicios en SAP para dar inicio al proceso administrativo respectivo.

            </span>

            </p>

    </div>


<div id="ACCION3" class="step slide_PDVSA" data-x="7000" data-y="-1500">

 <p align="justify">
        <span class="fonts_content_presentation_main">

            <br>
        <strong>Acciones</strong>
         <br>
            <br>

               4. Que las solicitudes de diferimiento de la edad de jubilación estén dirigidas únicamente al personal gerencial y personal del área operativa - medular del negocio.

            </p></span>

</div>


<div id="PROCESADAS1" class="step slide_PDVSA" data-x="8000" data-y="-1500">
<strong>Jubilaciones procesadas</strong><br><br>
<img src="../../static/images/resumenjubilaciones.png" height="470" width="650" />
</div>

<div id="PROCESADAS2" class="step slide_PDVSA" data-x="9000" data-y="-1500">
<strong>Jubilaciones ejecutadas</strong><br><br>
<img src="../../static/images/cuadro.png" height="430" width="800" />
</div>





<!----------------- SLIDE #6 ------------------>

     <div id="CVPMAIN" class="step slide_PDVSA" data-x="10000" data-y="-1500">

<?php

$sql = $conn->query("SELECT COUNT(*) FROM tbl_trabajadores_tipo_caso_EEMM WHERE tbl_trabajadores_tipo_caso_EEMM_org_eemm = 'CVP' ");
$row = $sql->fetch_row();
$count99 = $row[0];




        ?>

           <br>



         <span class="fonts_content_presentation_main">
         <strong>1. Corporación Venezolana de Petróleo<br></strong>
         Total de personal jubilable

        <br>

        <table>
            <tr>
                <td width="70%" valign="middle">
                    <br>
                        <ul style="list-style-type:square">
                            <li>
                            <a href="#CVP">CVP a nivel nacional: <?php echo $count99; ?></a>
                            <br>
                            </li>


                        </ul>
                </td>

            <tr>
        </table>
        </span>
        </center>



    </div>



<!----------------- SLIDE #9 ------------------>

    <div id="NNDD" class="step slide_PDVSA" data-x="11000" data-y="-1500">

<?php

$sql = $conn->query("SELECT COUNT(*) FROM tbl_trabajadores_tipo_caso_EEMM WHERE tbl_trabajadores_tipo_caso_EEMM_id_org_eemm = 10 ");
$row = $sql->fetch_row();
$count10 = $row[0];


$sql = $conn->query("SELECT COUNT(*) FROM tbl_trabajadores_tipo_caso_EEMM WHERE tbl_trabajadores_tipo_caso_EEMM_id_org_eemm = 13 ");
$row = $sql->fetch_row();
$count13 = $row[0];


$sql = $conn->query("SELECT COUNT(*) FROM tbl_trabajadores_tipo_caso_EEMM WHERE tbl_trabajadores_tipo_caso_EEMM_id_org_eemm = 17 ");
$row = $sql->fetch_row();
$count17 = $row[0];

$sql = $conn->query("SELECT COUNT(*) FROM tbl_trabajadores_tipo_caso_EEMM WHERE tbl_trabajadores_tipo_caso_EEMM_id_org_eemm = 19 ");
$row = $sql->fetch_row();
$count19 = $row[0];

$sql = $conn->query("SELECT COUNT(*) FROM tbl_trabajadores_tipo_caso_EEMM WHERE tbl_trabajadores_tipo_caso_EEMM_id_org_eemm = 21 ");
$row = $sql->fetch_row();
$count21 = $row[0];

$sql = $conn->query("SELECT COUNT(*) FROM tbl_trabajadores_tipo_caso_EEMM WHERE tbl_trabajadores_tipo_caso_EEMM_id_org_eemm = 28 ");
$row = $sql->fetch_row();
$count28 = $row[0];


        ?>

           <br>



         <span class="fonts_content_presentation_main">
         <strong>2. Empresas Mixtas Nuevos Desarrollos<br></strong>
         Total de personal jubilable por empresa

        <br>

        <table>
            <tr>
                <td width="70%" valign="middle">
                    <br>
                        <ul style="list-style-type:square">
                            <li>
                            <a href="#Petrobicentenario">Petrobicentenario: <?php echo $count10; ?></a>
                            <br>
                            </li>
                            <li>
                            <a href="#Petrocarabobo">Petrocarabobo: <?php echo $count13; ?></a>
                            <br>
                            </li>
                            <li>
                            <a href="#Petroindependencia">Petroindependencia: <?php echo $count17; ?> </a>
                            <br>
                            </li>
                            <li>
                            <a href="#Petrojunin">Petrojunín: <?php echo $count19; ?> </a>
                            <br>
                            </li>
                            <li>
                            <a href="#Petromacareo">Petromacareo: 0</a>
                            <br>
                            </li>

                        </ul>
                </td>
                <td valign="top">
                        <br>
                        <ul style="list-style-type:square">

                            <li>
                            <a href="#Petromiranda">Petromiranda: <?php echo $count21; ?> </a>
                            <br>
                            </li>
                            <li>
                            <a href="#Petrourica">Petrourica: <?php echo $count28; ?> </a>
                            <br>
                            </li>
                            <li>
                            <a href="#Petrovictoria">Petrovictoria: 0</a>
                            <br>
                            </li>

                        </ul>
                </td>
            <tr>
        </table>
        </span>
        </center>

    </div>

<!----------------- SLIDE #10 ------------------>
  <div id= "DOSAREAS" class="step slide_PDVSA" data-x="12000" data-y="-1500">

  <?php

$sql = $conn->query("SELECT COUNT(*) FROM tbl_trabajadores_tipo_caso_EEMM WHERE tbl_trabajadores_tipo_caso_EEMM_id_org_eemm = 28 ");
$row = $sql->fetch_row();
$count29 = $row[0];


$sql = $conn->query("SELECT COUNT(*) FROM tbl_trabajadores_tipo_caso_EEMM WHERE tbl_trabajadores_tipo_caso_EEMM_id_org_eemm = 5 ");
$row = $sql->fetch_row();
$count5 = $row[0];




        ?>

 <br>



         <span class="fonts_content_presentation_main">
         <strong>3. Empresas Mixtas Dos Áreas Geográficas<br></strong>
         Total de personal jubilable por empresa

        <br>

        <table>
            <tr>
                <td width="70%" valign="middle">
                    <br>
                        <ul style="list-style-type:square">
                            <li>
                            <a href="#Sinovenezolana">Sinovenezolana: 0</a>
                            <br>
                            </li>
                            <li>
                            <a href="#Petrowarao">Petrowarao: <?php echo $count29; ?></a>
                            <br>
                            </li>

                            <li>
                            <a href="#Petroquiriquire">Petroquiriquire: 0</a>
                            <br>
                            </li>
                            <li>
                            <a href="#Bielovenezolana">Bielovenezolana: <?php echo $count5; ?></a>
                            <br>
                            </li>

                        </ul>
                </td>
                <td valign="top">
                        <br>

                </td>
            <tr>
        </table>
        </span>
        </center>


    </div>



<!----------------- SLIDE #11 ------------------>
  <div id="FAJA" class="step slide_PDVSA" data-x="13000" data-y="-1500">

    <?php

$sql = $conn->query("SELECT COUNT(*) FROM tbl_trabajadores_tipo_caso_EEMM WHERE tbl_trabajadores_tipo_caso_EEMM_id_org_eemm = 22 ");
$row = $sql->fetch_row();
$count22 = $row[0];


$sql = $conn->query("SELECT COUNT(*) FROM tbl_trabajadores_tipo_caso_EEMM WHERE tbl_trabajadores_tipo_caso_EEMM_id_org_eemm = 15 ");
$row = $sql->fetch_row();
$count15 = $row[0];

$sql = $conn->query("SELECT COUNT(*) FROM tbl_trabajadores_tipo_caso_EEMM WHERE tbl_trabajadores_tipo_caso_EEMM_id_org_eemm = 14 ");
$row = $sql->fetch_row();
$count14 = $row[0];

$sql = $conn->query("SELECT COUNT(*) FROM tbl_trabajadores_tipo_caso_EEMM WHERE tbl_trabajadores_tipo_caso_EEMM_id_org_eemm = 8 ");
$row = $sql->fetch_row();
$count8 = $row[0];

$sql = $conn->query("SELECT COUNT(*) FROM tbl_trabajadores_tipo_caso_EEMM WHERE tbl_trabajadores_tipo_caso_EEMM_id_org_eemm = 24 ");
$row = $sql->fetch_row();
$count24 = $row[0];

$sql = $conn->query("SELECT COUNT(*) FROM tbl_trabajadores_tipo_caso_EEMM WHERE tbl_trabajadores_tipo_caso_EEMM_id_org_eemm = 26 ");
$row = $sql->fetch_row();
$count26 = $row[0];

$sql = $conn->query("SELECT COUNT(*) FROM tbl_trabajadores_tipo_caso_EEMM WHERE tbl_trabajadores_tipo_caso_EEMM_id_org_eemm = 20 ");
$row = $sql->fetch_row();
$count20 = $row[0];

$sql = $conn->query("SELECT COUNT(*) FROM tbl_trabajadores_tipo_caso_EEMM WHERE tbl_trabajadores_tipo_caso_EEMM_id_org_eemm = 32 ");
$row = $sql->fetch_row();
$count32 = $row[0];

$sql = $conn->query("SELECT COUNT(*) FROM tbl_trabajadores_tipo_caso_EEMM WHERE tbl_trabajadores_tipo_caso_EEMM_id_org_eemm = 35 ");
$row = $sql->fetch_row();
$count35 = $row[0];

$sql = $conn->query("SELECT COUNT(*) FROM tbl_trabajadores_tipo_caso_EEMM WHERE tbl_trabajadores_tipo_caso_EEMM_id_org_eemm = 9 ");
$row = $sql->fetch_row();
$count9 = $row[0];

$sql = $conn->query("SELECT COUNT(*) FROM tbl_trabajadores_tipo_caso_EEMM WHERE tbl_trabajadores_tipo_caso_EEMM_id_org_eemm = 16 ");
$row = $sql->fetch_row();
$count16 = $row[0];


$sql = $conn->query("SELECT COUNT(*) FROM tbl_trabajadores_tipo_caso_EEMM WHERE tbl_trabajadores_tipo_caso_EEMM_id_org_eemm = 34 ");
$row = $sql->fetch_row();
$count34 = $row[0];

$sql = $conn->query("SELECT COUNT(*) FROM tbl_trabajadores_tipo_caso_EEMM WHERE tbl_trabajadores_tipo_caso_EEMM_id_org_eemm = 36 ");
$row = $sql->fetch_row();
$count36 = $row[0];
        ?>
     <br>



         <span class="fonts_content_presentation_main">
         <strong>4. Empresas Mixtas Faja<br></strong>
         Total de personal jubilable por empresa

        <br>

        <table>
            <tr>
                <td width="38%" valign="middle">
                    <br>
                        <ul style="list-style-type:square">
                            <li>
                            <a href="#Petromonagas">Petromonagas: <?php echo $count22; ?></a>
                            <br>
                            </li>
                            <li>
                            <a href="#Petrodelta">Petrodelta: <?php echo $count15; ?></a>
                            <br>
                            </li>
                            <li>
                            <a href="#Petrocedeno">Petrocedeño: <?php echo $count14; ?></a>
                            <br>
                            </li>
                            <li>
                            <a href="#Indovenezolana">Indovenezolana: <?php echo $count8; ?></a>
                            <br>
                            </li>
                            <li>
                            <a href="#Petropiar">Petropiar: <?php echo $count24; ?></a>
                            <br>
                            </li>
                            <li>
                            <a href="#Petroritupano">Petroritupano: <?php echo $count26; ?></a>
                            <br>
                            </li>

                        </ul>
                </td>

                <td width="38%" valign="middle">
                       <br>
                        <ul style="list-style-type:square">
                            <li>
                            <a href="#Petrokarina">Petrokariña: 0</a>
                            <br>
                            </li>
                            <li>
                            <a href="#PetroleraKaki">Petrolera Kaki: <?php echo $count20; ?></a>
                            <br>
                            </li>
                            <li>
                            <a href="#Petrocuragua">Petrocuragua: 0</a>
                            <br>
                            </li>
                            <li>
                            <a href="#Petrovenbras">Petrovenbras: 0</a>
                            <br>
                            </li>
                            <li>
                            <a href="#Petrozumano">Petrozumano: <?php echo $count32; ?></a>
                            <br>
                            </li>
                            <li>
                            <a href="#Venangocupet">Venangocupet: <?php echo $count35; ?></a>
                            <br>
                            </li>

                        </ul>

                </td>
                <td width="48%" valign="middle">
                       <br>
                        <ul style="list-style-type:square">
                            <li>
                            <a href="#PetroSanFelix">Petro San Félix: <?php echo $count9; ?></a>
                            <br>
                            </li>
                            <li>
                            <a href="#Petroguarico">Petroguárico: <?php echo $count16; ?></a>
                            <br>
                            </li>
                            <li>
                            <a href="#Sinovensa">Sinovensa: <?php echo $count34; ?></a>
                            <br>
                            </li>
                            <li>
                            <a href="#Vencupet">Vencupet: <?php echo $count36; ?></a>
                            <br>
                            </li>



                        </ul>

                </td>
            <tr>
        </table>
        </span>
        </center>


    </div>

<!----------------- SLIDE #12 ------------------>
  <div id="OCCIDENTE" class="step slide_PDVSA" data-x="14000" data-y="-1500">
          <br>

        <?php


        $sql = $conn->query("SELECT COUNT(*) FROM tbl_trabajadores_tipo_caso_EEMM WHERE tbl_trabajadores_tipo_caso_EEMM_id_org_eemm = 11 ");
        $row = $sql->fetch_row();
        $count11 = $row[0];

        $sql = $conn->query("SELECT COUNT(*) FROM tbl_trabajadores_tipo_caso_EEMM WHERE tbl_trabajadores_tipo_caso_EEMM_id_org_eemm = 12 ");
        $row = $sql->fetch_row();
        $count12 = $row[0];

        $sql = $conn->query("SELECT COUNT(*) FROM tbl_trabajadores_tipo_caso_EEMM WHERE tbl_trabajadores_tipo_caso_EEMM_id_org_eemm = 18 ");
        $row = $sql->fetch_row();
        $count18 = $row[0];

        $sql = $conn->query("SELECT COUNT(*) FROM tbl_trabajadores_tipo_caso_EEMM WHERE tbl_trabajadores_tipo_caso_EEMM_id_org_eemm = 23 ");
        $row = $sql->fetch_row();
        $count23 = $row[0];

        $sql = $conn->query("SELECT COUNT(*) FROM tbl_trabajadores_tipo_caso_EEMM WHERE tbl_trabajadores_tipo_caso_EEMM_id_org_eemm = 25 ");
        $row = $sql->fetch_row();
        $count25 = $row[0];

        $sql = $conn->query("SELECT COUNT(*) FROM tbl_trabajadores_tipo_caso_EEMM WHERE tbl_trabajadores_tipo_caso_EEMM_id_org_eemm = 30 ");
        $row = $sql->fetch_row();
        $count30 = $row[0];

        $sql = $conn->query("SELECT COUNT(*) FROM tbl_trabajadores_tipo_caso_EEMM WHERE tbl_trabajadores_tipo_caso_EEMM_id_org_eemm = 31 ");
        $row = $sql->fetch_row();
        $count31 = $row[0];

        ?>

         <span class="fonts_content_presentation_main">
         <strong>5. Empresas Mixtas Occidente<br></strong>
        Total de personal jubilable por empresa


        <br>

        <table>
            <tr>
                <td width="60%" valign="middle">
                    <br>
                        <ul style="list-style-type:square">
                            <li>
                            <a href="#Baripetrol">Baripetrol: 0</a>
                            <br>
                            </li>
                            <li>
                            <a href="#Lagopetrol">Lagopetrol: 0</a>
                            <br>
                            </li>
                            <li>
                            <a href="#Petroboscan">Petroboscán: <?php echo $count11; ?></a>
                            <br>
                            </li>
                            <li>
                            <a href="#Petrocabimas">Petrocabimas: <?php echo $count12; ?></a>
                            <br>
                            </li>
                            <li>
                            <a href="#Petrocumarebo">Petrocumarebo: 0</a>
                            <br>
                            </li>
                            <li>
                            <a href="#Petroindependiente">Petroindependiente: <?php echo $count18; ?></a>
                            <br>
                            </li>

                        </ul>
                </td>

                <td width="40%" valign="middle">
                       <br>
                        <ul style="list-style-type:square">
                            <li>
                            <a href="#Petroperija">Petroperijá: <?php echo $count23; ?></a>
                            <br>
                            </li>
                            <li>
                            <a href="#PetroregionalLago">Petroregional del Lago: <?php echo $count25; ?></a>
                            <br>
                            </li>
                            <li>
                            <a href="#Petrourdaneta">Petrourdaneta: 0</a>
                            <br>
                            </li>
                            <li>
                            <a href="#Petrowayuu">Petrowayuú: <?php echo $count30; ?></a>
                            <br>
                            </li>
                            <li>
                            <a href="#Petrozamora">Petrozamora: <?php echo $count31; ?></a>
                            <br>
                            </li>


                        </ul>

                </td>

            <tr>
        </table>
        </span>
        </center>
    </div>




<!----------------- SLIDE #13 ------------------>
      <div id ="ORIENTE" class="step slide_PDVSA" data-x="15000" data-y="-1500">
          <br>

         <?php


        $sql = $conn->query("SELECT COUNT(*) FROM tbl_trabajadores_tipo_caso_EEMM WHERE tbl_trabajadores_tipo_caso_EEMM_id_org_eemm = 6 ");
        $row = $sql->fetch_row();
        $count6 = $row[0];

        $sql = $conn->query("SELECT COUNT(*) FROM tbl_trabajadores_tipo_caso_EEMM WHERE tbl_trabajadores_tipo_caso_EEMM_id_org_eemm = 27 ");
        $row = $sql->fetch_row();
        $count27 = $row[0];


        ?>

         <span class="fonts_content_presentation_main">
         <strong>6. Empresas Mixtas Oriente<br></strong>
         Total de personal jubilable por empresa


        <br>

        <table>
            <tr>
                <td width="60%" valign="middle">
                    <br>
                        <ul style="list-style-type:square">
                            <li>
                            <a href="#Boqueron">Boquerón: <?php echo $count6; ?></a>
                            <br>
                            </li>
                            <li>
                            <a href="#Petroleraparia">Petrolera Paria: 0</a>
                            <br>
                            </li>
                            <li>
                            <a href="#Petrosucre">Petrosucre: <?php echo $count27; ?></a>
                            <br>
                            </li>
                            <li>
                            <a href="#Petroguiria">Petrogüiria: 0</a>
                            <br>

                            </li>

                        </ul>
                </td>


            <tr>
        </table>
        </span>
        </center>
    </div>


    <!----------------- SLIDE #14 ------------------>
    <div id="Petrobicentenario" class="step slide_PDVSA" data-x="16000" data-y="-1500">
    EEMM Nuevos Desarrollos: Petrobicentenario<br>
    Detalle de jubilable(s):

    <?php
    if ($count10 = 1) {
        echo $count10 . " trabajador (a)";  }
    else{
        echo $count10 . " trabajadores (as)";  }
        ?>


        <br><br>

        <iframe id ="myChart" width="100%" height="69%" src="jubilables.php?id=10" scrolling="no">
         <p>Your browser does not support iframes.</p>
        </iframe>


       <br>

        <table width="100%">
            <tr>
                <td width="95%"></td>
                <td><a href="#NNDD"><img src="../../static/images/back.png" width="30" height="30"> </a></td>

            </tr>


        </table>



    </div>


    <div id="Petrocarabobo" class="step slide_PDVSA" data-x="17000" data-y="-1500">



     EEMM Nuevos Desarrollos: Petrocarabobo<br>
           Detalle de jubilable(s):

    <?php
    if ($count13 == 1) {
        echo $count13 . " trabajador (a)";  }
    else{
        echo $count13 . " trabajadores (as)";  }
        ?>


        <br><br>

            <iframe id ="myChart" width="100%" height="69%" src="jubilables.php?id=13" scrolling="no">
         <p>Your browser does not support iframes.</p>
        </iframe>


       <br>

        <table width="100%">
            <tr>
                <td width="95%"></td>
                <td><a href="#NNDD"><img src="../../static/images/back.png" width="30" height="30"> </a></td>

            </tr>


        </table>



    </div>

    <div id="Petroindependencia" class="step slide_PDVSA" data-x="18000" data-y="-1500">


    EEMM Nuevos Desarrollos: Petroindependencia<br>
        Detalle de jubilable(s):

    <?php
    if ($count17 == 1) {
        echo $count17 . " trabajador (a)";  }
    else{
        echo $count17 . " trabajadores (as)";  }
        ?>


        <br><br>

            <iframe id ="myChart" width="100%" height="69%" src="jubilables.php?id=17" scrolling="yes">
         <p>Your browser does not support iframes.</p>
        </iframe>


       <br>

        <table width="100%">
            <tr>
                <td width="95%"></td>
                <td><a href="#NNDD"><img src="../../static/images/back.png" width="30" height="30"> </a></td>

            </tr>


        </table>


    </div>


    <div id="Petrojunin" class="step slide_PDVSA" data-x="19000" data-y="-1500">


    EEMM Nuevos Desarrollos: Petrojunín<br>
     Detalle de jubilable(s):

    <?php
    if ($count19 == 1) {
        echo $count19 . " trabajador (a)";  }
    else{
        echo $count19 . " trabajadores (as)";  }
        ?>


        <br><br>

            <iframe id ="myChart" width="100%" height="69%" src="jubilables.php?id=19" scrolling="yes">
         <p>Your browser does not support iframes.</p>
        </iframe>


       <br>

        <table width="100%">
            <tr>
                <td width="95%"></td>
                <td><a href="#NNDD"><img src="../../static/images/back.png" width="30" height="30"> </a></td>

            </tr>


        </table>



    </div>



     <div id="Petromiranda" class="step slide_PDVSA" data-x="20000" data-y="-1500">


    EEMM Nuevos Desarrollos: Petromiranda<br>
        Detalle de jubilable(s):

    <?php
    if ($count21 == 1) {
        echo $count21 . " trabajador (a)";  }
    else{
        echo $count21 . " trabajadores (as)";  }
        ?>


        <br><br>

            <iframe id ="myChart" width="100%" height="69%" src="jubilables.php?id=21" scrolling="yes">
         <p>Your browser does not support iframes.</p>
        </iframe>


       <br>

        <table width="100%">
            <tr>
                <td width="95%"></td>
                <td><a href="#NNDD"><img src="../../static/images/back.png" width="30" height="30"> </a></td>

            </tr>


        </table>



    </div>
<!--
    <div id="Petroindependecia" class="step slide_PDVSA" data-x="16000" data-y="-1500">


    EEMM Nuevos Desarrollos: Petroindependencia<br>
       Detalle de jubilable(s):

    <?php
    if ($count6 == 1) {
        echo $count6 . " trabajador (a)";  }
    else{
        echo $count6 . " trabajadores (as)";  }
        ?>


        <br><br>

            <iframe id ="myChart" width="100%" height="69%" src="petroindependencia.php" scrolling="yes">
         <p>Your browser does not support iframes.</p>
        </iframe>


       <br>

        <table width="100%">
            <tr>
                <td width="95%"></td>
                <td><a href="#DOSAREAS"><img src="../../static/images/back.png" width="30" height="30"> </a></td>

            </tr>


        </table>



    </div>

    -->

    <div id="Bielovenezolana" class="step slide_PDVSA" data-x="21000" data-y="-1500">


    EEMM Dos Áreas Geográficas: Bielovenezolana<br>
       Detalle de jubilable(s):

    <?php
    if ($count5 == 1) {
        echo $count5 . " trabajador (a)";  }
    else{
        echo $count5 . " trabajadores (as)";  }
        ?>


        <br><br>

<iframe id ="myChart" width="100%" height="69%" src="jubilables.php?id=5" scrolling="yes">
         <p>Your browser does not support iframes.</p>
        </iframe>


        <table width="100%">
            <tr>
                <td width="95%"></td>
                <td><a href="#DOSAREAS"><img src="../../static/images/back.png" width="30" height="30"> </a></td>

            </tr>


        </table>



    </div>


    <div id="Petromonagas" class="step slide_PDVSA" data-x="22000" data-y="-1500">


    EEMM Faja: Petromonagas<br>
       Detalle de jubilable(s):

    <?php
    if ($count22 == 1) {
        echo $count22 . " trabajador (a)";  }
    else{
        echo $count22 . " trabajadores (as)";  }
        ?>


        <br><br>


<iframe id ="myChart" width="100%" height="69%" src="jubilables.php?id=22" scrolling="yes">         <p>Your browser does not support iframes.</p>
        </iframe>


       <br>

        <table width="100%">
            <tr>
                <td width="95%"></td>
                <td><a href="#FAJA"><img src="../../static/images/back.png" width="30" height="30"> </a></td>

            </tr>


        </table>




    </div>

    <div id="Petrodelta" class="step slide_PDVSA" data-x="23000" data-y="-1500">


    EEMM Faja: Petrodelta<br>
        Detalle de jubilable(s):

    <?php
    if ($count15 == 1) {
        echo $count15 . " trabajador (a)";  }
    else{
        echo $count15 . " trabajadores (as)";  }
        ?>


        <br><br>

           <iframe id ="myChart" width="100%" height="69%" src="jubilables.php?id=15" scrolling="yes">
         <p>Your browser does not support iframes.</p>
        </iframe>


       <br>

        <table width="100%">
            <tr>
                <td width="95%"></td>
                <td><a href="#FAJA"><img src="../../static/images/back.png" width="30" height="30"> </a></td>

            </tr>


        </table>




    </div>


    <div id="Petrocedeno" class="step slide_PDVSA" data-x="24000" data-y="-1500">
    EEMM Faja: Petrocedeño<br>
        Detalle de jubilable(s):

    <?php
    if ($count14 == 1) {
        echo $count14 . " trabajador (a)";  }
    else{
        echo $count14 . " trabajadores (as)";  }
        ?>


        <br><br>
            <iframe id ="myChart" width="100%" height="69%" src="jubilables.php?id=14" scrolling="yes">
            <p>Your browser does not support iframes.</p>
        </iframe>
        <br>
        <table width="100%">
            <tr>
                <td width="95%"></td>
                <td><a href="#FAJA"><img src="../../static/images/back.png" width="30" height="30"> </a></td>
            </tr>
        </table>
    </div>


    <div id="Indovenezolana" class="step slide_PDVSA" data-x="25000" data-y="-1500">
    EEMM Faja: Indovenezolana<br>
        Detalle de jubilable(s):

    <?php
    if ($count8 == 1) {
        echo $count8 . " trabajador (a)";  }
    else{
        echo $count8 . " trabajadores (as)";  }
        ?>


        <br><br>
            <iframe id ="myChart" width="100%" height="69%" src="jubilables.php?id=8" scrolling="yes">
            <p>Your browser does not support iframes.</p>
        </iframe>
        <br>
        <table width="100%">
            <tr>
                <td width="95%"></td>
                <td><a href="#FAJA"><img src="../../static/images/back.png" width="30" height="30"> </a></td>
            </tr>
        </table>
    </div>

   <div id="Petropiar" class="step slide_PDVSA" data-x="26000" data-y="-1500">
    EEMM Faja: Petropiar<br>

    Detalle de jubilable(s):

    <?php
    if ($count24 == 1) {
        echo $count24 . " trabajador (a)";  }
    else{
        echo $count24 . " trabajadores (as)";  }
        ?>


        <br><br>
  <iframe id ="myChart" width="100%" height="69%" src="jubilables.php?id=24" scrolling="yes">
            <p>Your browser does not support iframes.</p>
        </iframe>
        <br>
        <table width="100%">
            <tr>
                <td width="95%"></td>
                <td><a href="#FAJA"><img src="../../static/images/back.png" width="30" height="30"> </a></td>
            </tr>
        </table>
    </div>


       <div id="Petroritupano" class="step slide_PDVSA" data-x="27000" data-y="-1500">
    EEMM Faja: Petroritupano<br>
        Detalle de jubilable(s):

    <?php
    if ($count26 == 1) {
        echo $count26 . " trabajador (a)";  }
    else{
        echo $count26 . " trabajadores (as)";  }
        ?>


        <br><br>
    <iframe id ="myChart" width="100%" height="69%" src="jubilables.php?id=26" scrolling="yes">
            <p>Your browser does not support iframes.</p>
        </iframe>
        <br>
        <table width="100%">
            <tr>
                <td width="95%"></td>
                <td><a href="#FAJA"><img src="../../static/images/back.png" width="30" height="30"> </a></td>
            </tr>
        </table>
    </div>



      <div id="PetroleraKaki" class="step slide_PDVSA" data-x="28000" data-y="-1500">
    EEMM Faja: Petrolera Kaki<br>
        Detalle de jubilable(s):

    <?php
    if ($count20 == 1) {
        echo $count20 . " trabajador (a)";  }
    else{
        echo $count20 . " trabajadores (as)";  }
        ?>


        <br><br>
            <iframe id ="myChart" width="100%" height="69%" src="jubilables.php?id=20" scrolling="yes">
            <p>Your browser does not support iframes.</p>
        </iframe>
        <br>
        <table width="100%">
            <tr>
                <td width="95%"></td>
                <td><a href="#FAJA"><img src="../../static/images/back.png" width="30" height="30"> </a></td>
            </tr>
        </table>
    </div>


   <div id="Petrozumano" class="step slide_PDVSA" data-x="29000" data-y="-1500">
    EEMM Faja: Petrozumano<br>
        Detalle de jubilable(s):

    <?php
    if ($count32 == 1) {
        echo $count32 . " trabajador (a)";  }
    else{
        echo $count32 . " trabajadores (as)";  }
        ?>


        <br><br>

            <iframe id ="myChart" width="100%" height="69%" src="jubilables.php?id=32" scrolling="yes">

            <p>Your browser does not support iframes.</p>
        </iframe>
        <br>
        <table width="100%">
            <tr>
                <td width="95%"></td>
                <td><a href="#FAJA"><img src="../../static/images/back.png" width="30" height="30"> </a></td>
            </tr>
        </table>
    </div>


        <div id="Venangocupet" class="step slide_PDVSA" data-x="30000" data-y="-1500">
    EEMM Faja: Venangocupet<br>
        Detalle de jubilable(s):

    <?php
    if ($count35 == 1) {
        echo $count35 . " trabajador (a)";  }
    else{
        echo $count35 . " trabajadores (as)";  }
        ?>


        <br><br>
            <iframe id ="myChart" width="100%" height="69%" src="jubilables.php?id=35" scrolling="no">
            <p>Your browser does not support iframes.</p>
        </iframe>
        <br>
        <table width="100%">
            <tr>
                <td width="95%"></td>
                <td><a href="#FAJA"><img src="../../static/images/back.png" width="30" height="30"> </a></td>
            </tr>
        </table>
    </div>


    <div id="PetroSanFelix" class="step slide_PDVSA" data-x="31000" data-y="-1500">
    EEMM Faja: Petro San Félix<br>
        Detalle de jubilable(s):

    <?php
    if ($count9 == 1) {
        echo $count9 . " trabajador (a)";  }
    else{
        echo $count9 . " trabajadores (as)";  }
        ?>


        <br><br>
     <iframe id ="myChart" width="100%" height="69%" src="jubilables.php?id=9" scrolling="yes">
            <p>Your browser does not support iframes.</p>
        </iframe>
        <br>
        <table width="100%">
            <tr>
                <td width="95%"></td>
                <td><a href="#FAJA"><img src="../../static/images/back.png" width="30" height="30"> </a></td>
            </tr>
        </table>
    </div>


    <div id="Petroguarico" class="step slide_PDVSA" data-x="32000" data-y="-1500">
    EEMM Faja: Petroguárico<br>
        Detalle de jubilable(s):

    <?php
    if ($count16 == 1) {
        echo $count16 . " trabajador (a)";  }
    else{
        echo $count16 . " trabajadores (as)";  }
        ?>


        <br><br>
            <iframe id ="myChart" width="100%" height="69%" src="jubilables.php?id=16" scrolling="yes">
            <p>Your browser does not support iframes.</p>
        </iframe>
        <br>
        <table width="100%">
            <tr>
                <td width="95%"></td>
                <td><a href="#FAJA"><img src="../../static/images/back.png" width="30" height="30"> </a></td>
            </tr>
        </table>
    </div>

     <div id="Sinovensa" class="step slide_PDVSA" data-x="33000" data-y="-1500">
    EEMM Faja: Sinovensa<br>
        Detalle de jubilable(s):

    <?php
    if ($count34 == 1) {
        echo $count34 . " trabajador (a)";  }
    else{
        echo $count34 . " trabajadores (as)";  }
        ?>


        <br><br>

<iframe id ="myChart" width="100%" height="69%" src="jubilables.php?id=34" scrolling="yes">            <p>Your browser does not support iframes.</p>
        </iframe>
        <br>
        <table width="100%">
            <tr>
                <td width="95%"></td>
                <td><a href="#FAJA"><img src="../../static/images/back.png" width="30" height="30"> </a></td>
            </tr>
        </table>
    </div>

    <div id="Vencupet" class="step slide_PDVSA" data-x="34000" data-y="-1500">
    EEMM Faja: Vencupet<br>
        Detalle de jubilable(s):

    <?php
    if ($count36 == 1) {
        echo $count36 . " trabajador (a)";  }
    else{
        echo $count36 . " trabajadores (as)";  }
        ?>


        <br><br>
<iframe id ="myChart" width="100%" height="69%" src="jubilables.php?id=36" scrolling="yes">            <p>Your browser does not support iframes.</p>
        </iframe>
        <br>
        <table width="100%">
            <tr>
                <td width="95%"></td>
                <td><a href="#FAJA"><img src="../../static/images/back.png" width="30" height="30"> </a></td>
            </tr>
        </table>
    </div>


    <div id="Petroboscan" class="step slide_PDVSA" data-x="35000" data-y="-1500">
    EEMM Occidente: Petroboscán<br>
    Detalle de jubilable(s):

    <?php
    if ($count11 == 1) {
        echo $count11 . " trabajador (a)";  }
    else{
        echo $count11 . " trabajadores (as)";  }
        ?>


        <br><br>
            <iframe id ="myChart" width="100%" height="69%" src="jubilables.php?id=11" scrolling="yes">
            <p>Your browser does not support iframes.</p>
        </iframe>
        <br>
        <table width="100%">
            <tr>
                <td width="95%"></td>
                <td><a href="#OCCIDENTE"><img src="../../static/images/back.png" width="30" height="30"> </a></td>
            </tr>
        </table>
    </div>


    <div id="Petrocabimas" class="step slide_PDVSA" data-x="36000" data-y="-1500">
    EEMM Occidente: Petrocabimas<br>
        Detalle de jubilable(s):

    <?php
    if ($count12 == 1) {
        echo $count12 . " trabajador (a)";  }
    else{
        echo $count12 . " trabajadores (as)";  }
        ?>


        <br><br>


            <iframe id ="myChart" width="100%" height="69%" src="jubilables.php?id=12" scrolling="yes">
            <p>Your browser does not support iframes.</p>
        </iframe>
        <br>
        <table width="100%">
            <tr>
                <td width="95%"></td>
                <td><a href="#OCCIDENTE"><img src="../../static/images/back.png" width="30" height="30"> </a></td>
            </tr>
        </table>
    </div>


<div id="Petroindependiente" class="step slide_PDVSA" data-x="37000" data-y="-1500">
    EEMM Occidente: Petroindependiente<br>
       Detalle de jubilable(s):

    <?php
    if ($count17 == 1) {
        echo $count18 . " trabajador (a)";  }
    else{
        echo $count18 . " trabajadores (as)";  }
        ?>


        <br><br>
            <iframe id ="myChart" width="100%" height="69%" src="jubilables.php?id=18" scrolling="yes">
            <p>Your browser does not support iframes.</p>
        </iframe>
        <br>
        <table width="100%">
            <tr>
                <td width="95%"></td>
                <td><a href="#OCCIDENTE"><img src="../../static/images/back.png" width="30" height="30"> </a></td>
            </tr>
        </table>
    </div>

    <div id="Petroperija" class="step slide_PDVSA" data-x="38000" data-y="-1500">
    EEMM Occidente: Petroperijá<br>

    Detalle de jubilable(s):

    <?php
    if ($count23 == 1) {
        echo $count23 . " trabajador (a)";  }
    else{
        echo $count23 . " trabajadores (as)";  }
        ?>


        <br><br>
            <iframe id ="myChart" width="100%" height="69%" src="jubilables.php?id=23" scrolling="yes">
            <p>Your browser does not support iframes.</p>
        </iframe>
        <br>
        <table width="100%">
            <tr>
                <td width="95%"></td>
                <td><a href="#OCCIDENTE"><img src="../../static/images/back.png" width="30" height="30"> </a></td>
            </tr>
        </table>
    </div>





 <div id="PetroregionalLago" class="step slide_PDVSA" data-x="39000" data-y="-1500">
    EEMM Occidente: Petroregional del Lago<br>
        Detalle de jubilable(s):

    <?php
    if ($count25 == 1) {
        echo $count25 . " trabajador (a)";  }
    else{
        echo $count25 . " trabajadores (as)";  }
        ?>


        <br><br>
            <iframe id ="myChart" width="100%" height="69%" src="jubilables.php?id=25" scrolling="yes">
            <p>Your browser does not support iframes.</p>
        </iframe>
        <br>
        <table width="100%">
            <tr>
                <td width="95%"></td>
                <td><a href="#OCCIDENTE"><img src="../../static/images/back.png" width="30" height="30"> </a></td>
            </tr>
        </table>
    </div>





 <div id="Petrowayuu" class="step slide_PDVSA" data-x="40000" data-y="-1500">
    EEMM Occidente: Petrowayuú<br>
        Detalle de jubilable(s):

    <?php
    if ($count6 == 1) {
        echo $count30 . " trabajador (a)";  }
    else{
        echo $count30 . " trabajadores (as)";  }
        ?>


        <br><br>
            <iframe id ="myChart" width="100%" height="69%" src="jubilables.php?id=30" scrolling="no">
            <p>Your browser does not support iframes.</p>
        </iframe>
        <br>
        <table width="100%">
            <tr>
                <td width="95%"></td>
                <td><a href="#OCCIDENTE"><img src="../../static/images/back.png" width="30" height="30"> </a></td>
            </tr>
        </table>
    </div>



 <div id="Petrozamora" class="step slide_PDVSA" data-x="41000" data-y="-1500">
    EEMM Occidente: Petrozamora<br>
       Detalle de jubilable(s):

    <?php
    if ($count31 == 1) {
        echo $count31 . " trabajador (a)";  }
    else{
        echo $count31 . " trabajadores (as)";  }
        ?>


        <br><br>
            <iframe id ="myChart" width="100%" height="69%" src="jubilables.php?id=31" scrolling="yes">
            <p>Your browser does not support iframes.</p>
        </iframe>
        <br>
        <table width="100%">
            <tr>
                <td width="95%"></td>
                <td><a href="#OCCIDENTE"><img src="../../static/images/back.png" width="30" height="30"> </a></td>
            </tr>
        </table>
    </div>



 <div id="Boqueron" class="step slide_PDVSA" data-x="42000" data-y="-1500">
    EEMM Oriente: Boquerón<br>

    Detalle de jubilable(s):

    <?php
    if ($count6 == 1) {
        echo $count6 . " trabajador (a)";  }
    else{
        echo $count6 . " trabajadores (as)";  }
        ?>


        <br><br>


        <iframe id ="myChart" width="100%" height="69%" src="jubilables.php?id=6" scrolling="yes">
        <p>Your browser does not support iframes.</p>
        </iframe>
        <br>
        <table width="100%">
            <tr>
                <td width="95%"></td>
                <td><a href="#ORIENTE"><img src="../../static/images/back.png" width="30" height="30"> </a></td>
            </tr>
        </table>
    </div>

 <div id="Petrosucre" class="step slide_PDVSA" data-x="43000" data-y="-1500">
    EEMM Oriente: Petrosucre<br>
        Detalle de jubilable(s):

    <?php
    if ($count27 == 1) {
        echo $count27 . " trabajador (a)";  }
    else{
        echo $count27 . " trabajadores (as)";  }
        ?>


        <br><br>
        <iframe id ="myChart" width="100%" height="69%" src="jubilables.php?id=27" scrolling="yes">
         <p>Your browser does not support iframes.</p>
        </iframe>
        <br>
        <table width="100%">
            <tr>
                <td width="95%"></td>
                <td><a href="#ORIENTE"><img src="../../static/images/back.png" width="30" height="30"> </a></td>
            </tr>
        </table>
    </div>


     <div id="CVP" class="step slide_PDVSA" data-x="44000" data-y="-1500">
    CVP a nivel nacional<br>
        Detalle de jubilable(s):

    <?php
    if ($count99 == 1) {
        echo $count99 . " trabajador (a)";  }
    else{
        echo $count99 . " trabajadores (as)";  }
        ?>


        <br><br>
        <iframe id ="myChart" width="100%" height="69%" src="jubilables.php?id=7" scrolling="yes">
         <p>Your browser does not support iframes.</p>
        </iframe>
        <br>
        <table width="100%">
            <tr>
                <td width="95%"></td>
                <td><a href="#CVPMAIN"><img src="../../static/images/back.png" width="30" height="30"> </a></td>
            </tr>
        </table>
    </div>











<div class="hint">
    <p>Presione espaciadora</p>
</div>
<script>
    if ("ontouchstart" in document.documentElement) {
        document.querySelector(".hint").innerHTML = "<p>Presione las flechas izquierda y/o derecha para retroceder/avanzar</p>";
    }
</script>

<script src="../../static/javascripts/impress.js-master/js/impress.js"></script>
<script>impress().init();</script>

</div>

    <div class="footer_main">

        <?php include '../includes/footer_presentation.html';?>
    </div>

</body>
</html>
