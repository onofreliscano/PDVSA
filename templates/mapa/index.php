<!DOCTYPE html>

<html class="no-js" lang="es">
<head>
    <title> Mapa De Venezuela para RRHH Corporativo</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="">
<meta name="viewport" content="width=800, user-scalable=no">



<link rel="stylesheet" href="https://openlayers.org/en/v4.6.5/css/ol.css" type="text/css">
    <style>
      .map {
        height: 500px;
        width: 100%;
      }
    </style>
    <script src="https://openlayers.org/en/v4.6.5/build/ol.js" type="text/javascript"></script>
   
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/openlayers/4.6.5/ol.css" type="text/css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/openlayers/4.6.5/ol.js" type="text/javascript"></script>


</head>



    <body class="impress-not-supported" >

<?php include '../includes/css-js.html';?>
<?php include '../includes/header_presentation.html';?>
<!--<script src="easytimer.min.js"></script>-->




<div id="impress">

  


  


<!----------------- SLIDE #1 ------------------>

    <div id = "slide1" class="step slide_PDVSA " data-x="1000" data-y="-1500">
      <p align="left">
        
    <strong>Mapa De Venezuela para RRHH Corporativo</strong>


    <div id="map" class="map"></div>
    <script type="text/javascript">
      var map = new ol.Map({
        target: 'map',
        layers: [
          new ol.layer.Tile({
            source: new ol.source.OSM()
          })
        ],
        view: new ol.View({
          center: ol.proj.fromLonLat([-66.91, 10.50]),
          zoom: 6
        })
      });
    </script>
    <br>
    <!--<button id="pan-to-london">Region Oriente</button>
    <button id="pan-to-london">Region Sur</button>
    <button id="pan-to-london">Region Occidente</button>-->
    
    </div>

    
              


<div class="hint">
    <p>Presione espaciadora</p>
</div>
<script>
    if ("ontouchstart" in document.documentElement) {
        document.querySelector(".hint").innerHTML = "<p>Presione las flechas izquierda y/o derecha para retroceder/avanzar</p>";
    }
</script>


<script>impress().init();</script>

</div>

    <div class="footer_main">

        <?php include '../includes/footer_presentation.html';?>
    </div>



</body>
</html>
